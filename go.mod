module github.com/adminfromhell/bot

require (
	cloud.google.com/go v0.32.0
	github.com/Jeffail/gabs v1.4.0 // indirect
	github.com/detached/gorocket v0.0.0-20170629192631-d44bbd3f26d2 // indirect
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible // indirect
	github.com/googleapis/gax-go v2.0.0+incompatible // indirect
	github.com/gopackage/ddp v0.0.0-20170117053602-652027933df4 // indirect
	github.com/gorilla/websocket v1.4.0 // indirect
	github.com/lusis/go-slackbot v0.0.0-20180109053408-401027ccfef5 // indirect
	github.com/lusis/slack-test v0.0.0-20190426140909-c40012f20018 // indirect
	github.com/mattn/go-shellwords v1.0.3
	github.com/mozillazg/go-unidecode v0.1.0
	github.com/nlopes/slack v0.4.0
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/pkg/errors v0.8.0 // indirect
	github.com/pyinx/gorocket v0.0.0-20170810024322-78ae1353729f
	github.com/robfig/cron v0.0.0-20180505203441-b41be1df6967
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.3.0 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/thoj/go-ircevent v0.0.0-20180816043103-14f3614f28c3
	go.opencensus.io v0.18.0 // indirect
	golang.org/x/net v0.0.0-20181108082009-03003ca0c849 // indirect
	golang.org/x/oauth2 v0.0.0-20180821212333-d2e6202438be
	golang.org/x/sync v0.0.0-20181108010431-42b317875d0f // indirect
	google.golang.org/api v0.0.0-20181108001712-cfbc873f6b93 // indirect
	google.golang.org/genproto v0.0.0-20181109154231-b5d43981345b // indirect
	gopkg.in/telegram-bot-api.v3 v3.0.0
)
